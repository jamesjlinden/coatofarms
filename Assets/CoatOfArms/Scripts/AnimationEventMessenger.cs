﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventMessenger : MonoBehaviour
{
    public GameObject receiver;

    public void OnIdleBegin()
    {
        receiver.SendMessage("OnIdleBegin");
    }
}
