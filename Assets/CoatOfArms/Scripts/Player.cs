﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour
{
    public CoatOfArmsCharacterController characterController;
	public Animator simpleGameplayRigAnimator;
    [NonSerialized] public bool comboCompleted;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !comboCompleted)
        {
            simpleGameplayRigAnimator.SetTrigger(SimpleGameplayRigParameters.lmbBuffered);
            characterController.canMove = false;
        }

        if (Input.GetMouseButtonDown(1) && !comboCompleted)
        {
            simpleGameplayRigAnimator.SetTrigger(SimpleGameplayRigParameters.rmbBuffered);
            characterController.canMove = false;
        }
    }

    public void OnIdleBegin()
    {
        ClearCombo();
    }

    public void ClearCombo()
    {
        comboCompleted = false;
        characterController.canMove = true;
    }

    public void CompleteCombo()
    {
        comboCompleted = false;
    }
}

public static class SimpleGameplayRigParameters
{
    public static int lmbBuffered = Animator.StringToHash("LMBBuffered");
    public static int rmbBuffered = Animator.StringToHash("RMBBuffered");
}