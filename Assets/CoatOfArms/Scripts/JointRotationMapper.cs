﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointRotationMapper : MonoBehaviour
{
    public List<MappedRotation> rotationMappings = new List<MappedRotation>();

    private void LateUpdate()
    {
        foreach (var rotationMapping in rotationMappings)
        {
            transform.Rotate(rotationMapping.GetRotationAxis(), rotationMapping.GetRotationAngle(), Space.World);
        }
    }

    public enum Axis
    {
        X = 0,
        Y,
        Z
    }

    [System.Serializable]
    public class MappedRotation
    {
        [SerializeField] public Transform axisToRotateAbout;
        [SerializeField] public Axis axisChannel;
        [SerializeField] public Transform angleToRotateAbout;
        [SerializeField] public Axis angleChannel;

        public Vector3 GetRotationAxis()
        {
            if (axisToRotateAbout && angleToRotateAbout)
            {
                switch (axisChannel)
                {
                    case Axis.X:
                        return axisToRotateAbout.right;
                    case Axis.Y:
                        return axisToRotateAbout.up;
                    case Axis.Z:
                        return axisToRotateAbout.forward;
                }
            }

            return default;
        }

        public float GetRotationAngle()
        {
            return angleToRotateAbout.transform.localEulerAngles[(int)angleChannel];
        }
    }
}